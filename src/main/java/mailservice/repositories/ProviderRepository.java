package mailservice.repositories;

import mailservice.entities.Provider;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ProviderRepository extends JpaRepository<Provider, Long> {

}