package mailservice.controllers;

import mailservice.entities.Provider;
import mailservice.services.ProviderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class ProviderController {

    private final ProviderService providerService;

    public ProviderController(ProviderService providerService) {
        this.providerService = providerService;
    }

    @GetMapping("providerTB/all")
    public ResponseEntity<List<Provider>> getAll(){
        List<Provider> listProvider = providerService.getAllProviderTB();
        return new ResponseEntity<List<Provider>>(listProvider, HttpStatus.OK);
    }
//
//    @GetMapping("providerTB/{id}")
//    public ResponseEntity<?> getStudent(@PathVariable("id") long id){
//        if (providerService.getProviderTB(id) == null) {
//            throw new ProviderTBNotFoundException("Not Found ProviderTB id-" + id);
//        } else {
//            Provider provider = providerService.getProviderTB(id);
//            return new ResponseEntity<Provider>(provider, HttpStatus.OK);
//        }
//    }
//
//    //Add new providerTB
//    @PostMapping("providerTB/add")
//    public ResponseEntity<?> addProvider(@RequestBody Provider provider){
//        Provider dataProvider = providerService.addProviderTB(provider);
//        if (dataProvider != null) {
//            return new ResponseEntity<Provider>(dataProvider, HttpStatus.OK);
//        }
//        throw new ProviderTBNotFoundException("Can not create providerTB");
//    }
//
//    @PutMapping("providerTB/{id}")
//    public ResponseEntity<?> updateProviderTB(@PathVariable("id") long id, @RequestBody Provider provider){
//        Provider dataProvider = providerService.getProviderTB(id);
//        if (dataProvider == null) {
//            throw new ProviderTBNotFoundException("Not Found ProviderTB id-" + id);
//        } else {
//            dataProvider.setName(provider.getName());
//            dataProvider.setDesc(provider.getDesc());
//            dataProvider.setAccessKey(provider.getAccessKey());
//            providerService.updateProviderTB(dataProvider);
//            return new ResponseEntity<Provider>(dataProvider, HttpStatus.OK);
//        }
//    }
//
//    @DeleteMapping("student/{id}")
//    public ResponseEntity<?> deleteProviderTB(@PathVariable("id") long id){
//        Provider dataStd = providerService.getProviderTB(id);
//        if (dataStd == null) {
//            throw new ProviderTBNotFoundException("Not Found ProviderTB id-" + id);
//        } else {
//            providerService.delProviderTB(id);
//            throw new ProviderTBNotFoundException("Delete ProviderTB id-" + id);
//        }
//    }
}
