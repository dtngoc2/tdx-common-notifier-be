package mailservice.utils.common.exception;

public class ProviderTBNotFoundException extends RuntimeException {

    public ProviderTBNotFoundException(String exception) {
        super(exception);
    }

}
