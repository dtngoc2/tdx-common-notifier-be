package mailservice.entities;

import lombok.*;
import java.util.Collection;
import jakarta.persistence.*;
import mailservice.entities.common.BaseEntity;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class MailTemplate extends BaseEntity {
    private String code;
    private String name;
    private String content;
    private String note;

    @OneToMany(mappedBy = "mailTemplate", cascade = CascadeType.ALL)
    private Collection<MailInfo> mailInfos;

    @ManyToOne
    @JoinColumn(name = "provider_id")
    private Provider provider;
}