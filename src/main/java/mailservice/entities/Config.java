package mailservice.entities;

import lombok.*;
import jakarta.persistence.*;
import mailservice.entities.common.BaseEntity;

@Entity
@Builder
@Getter @Setter @AllArgsConstructor
public class Config extends BaseEntity {

}
