package mailservice.entities;

import lombok.*;
import java.util.Collection;
import jakarta.persistence.*;
import mailservice.entities.common.BaseEntity;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Provider extends BaseEntity {
    private String name;
    private String accessKey;
    private String note;
    private String domain;
    private String description;
    private String image;
    @OneToMany(mappedBy = "provider", cascade = CascadeType.ALL)
    private Collection<MailInfo> mailInfos;
    @OneToMany(mappedBy = "provider", cascade = CascadeType.ALL)
    private Collection<MailTemplate> mailTemplates;
}