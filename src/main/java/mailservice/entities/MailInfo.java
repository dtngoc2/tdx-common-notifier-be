package mailservice.entities;

import lombok.*;
import jakarta.persistence.*;
import mailservice.entities.common.BaseEntity;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class MailInfo extends BaseEntity {
    private String mailFrom;
    private String mailTo;
    private String subject;
    private String content;
    private String status;
    private String note;

    @ManyToOne
    @JoinColumn(name = "provider_id")
    private Provider provider;
    @ManyToOne
    @JoinColumn(name = "template_id")
    private MailTemplate mailTemplate;
}