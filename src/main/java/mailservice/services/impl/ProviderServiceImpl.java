package mailservice.services.impl;

import mailservice.entities.Provider;
import mailservice.repositories.ProviderRepository;
import mailservice.services.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProviderServiceImpl implements ProviderService {
    private final ProviderRepository providerRepository;

    public ProviderServiceImpl(ProviderRepository providerRepository) {
        this.providerRepository = providerRepository;
    }

    @Override
    public List<Provider> getAllProviderTB() {
        List<Provider> listProvider = providerRepository.findAll();
        return listProvider;
    }
//
//    @Override
//    public Provider getProviderTB(long id) {
//        Provider provider = providerRepository.getOne(id);
//        return provider;
//    }
//
//    @Override
//    public Provider addProviderTB(Provider provider) {
//        return providerRepository.save(provider);
//    }
//
//    @Override
//    public Provider updateProviderTB(Provider provider) {
//        provider = providerRepository.save(provider);
//        return provider;
//    }
//
//    @Override
//    public boolean delProviderTB(long id) {
//        Provider provider = providerRepository.getOne(id);
//        if (provider != null) {
//            providerRepository.deleteById(id);
//            return true;
//        }
//        return false;
//    }
}
